# Docker container for web application
To deploy your web application (for example React or AngularJS) in a docker container you just need to follow steps below.

## 1. Dockerfile
Create a file named *Dockerfile* in your application root directory containing code from an example. 

It will instruct Docker to create new image and copy your application sources into it. 

```
COPY package.json /app/package.json
COPY . /app
```

Next it will install all required npm modules.
```
RUN npm install --silent
```

And finally build your app.

Then Docker will use the output to create another image. This image will host our build earlier application.

```
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
```

Finally when image is lunched web server daemon (nginx in this case) is turned on to provide our application. 
```
CMD ["nginx", "-g", "daemon off;"]
```

## 2. Build image
I assume that you have your docker available on your computer. If not go to (https://www.docker.com/)[https://www.docker.com/] to get started. 

You can build your docker image buy running *docker build* command. In our case we added it to application package.json scripts.

```
docker build -t docker_hub_account/your_site .
```

## 3. Test run

To run our container we can simply *docker run* it.

```
docker run -p 3000:80 -d --name your_site docker_hub_account/your_site
```

This will expose our web server at http:// localhost:3000.
